

import buslogic.clspln;
import buslogic.clsplnprp;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class plnserv extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      if(request.getParameter("btnsub")!=null)
      {
          clspln obj=new clspln();
          clsplnprp objprp=new clsplnprp();
          objprp.setplnnam(request.getParameter("txtpln"));
          objprp.setplnnoq(Integer.parseInt(request.getParameter("txtnoq")));
          objprp.setplncst(Float.parseFloat(request.getParameter("txtprc")));
          obj.save_rec(objprp);
      }
      if(request.getParameter("pcod")!=null)
      {
          clspln obj=new clspln();
          clsplnprp objprp=new clsplnprp();
          objprp.setplncod(Integer.parseInt(request.getParameter("pcod")));
          obj.del_rec(objprp);
      }
      if(request.getParameter("btnupd")!=null)
      {
          clspln obj=new clspln();
          clsplnprp objprp=new clsplnprp();
          objprp.setplnnam(request.getParameter("txtpln"));
          objprp.setplnnoq(Integer.parseInt(request.getParameter("txtnoq")));
          objprp.setplncst(Float.parseFloat(request.getParameter("txtprc")));
          HttpSession ses=request.getSession();
     objprp.setplncod(Integer.parseInt(ses.getAttribute("pcod").toString()));
          obj.upd_rec(objprp);
          ses.removeAttribute("pcod");
      }
      response.sendRedirect("admin/frmpln.jsp");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
