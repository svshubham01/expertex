import buslogic.clsexp;
import buslogic.clsexpprp;
import buslogic.clsusr;
import buslogic.clsusrprp;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;
import javax.mail.*;
import javax.activation.*;
import java.util.UUID;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class expserv extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if(request.getParameter("ecod")!=null)
        {
            clsexp obj=new clsexp();
            clsexpprp objprp=new clsexpprp();
            objprp.setexpcod(Integer.parseInt(request.getParameter("ecod")));
            obj.del_rec(objprp);
             response.sendRedirect("admin/frmexp.jsp");
        }
      if(request.getParameter("btnsub")!=null)
      {
          try
          {
          clsusr obj=new clsusr();
          clsusrprp objprp=new clsusrprp();
          objprp.setusreml(request.getParameter("txtexpeml"));
          objprp.setusrrol("E");
          String pwd=UUID.randomUUID().toString();
          pwd=pwd.substring(pwd.lastIndexOf("-")+1);
          objprp.setusrpwd(pwd);
          int ucod=obj.save_rec(objprp);
          //store record to tbexp
          clsexp obj1=new clsexp();
          clsexpprp objprp1=new clsexpprp();
          objprp1.setexpnam(request.getParameter("txtexpnam"));
          objprp1.setexppic("");
          objprp1.setexpprf("");
       java.sql.Date s = new java.sql.Date(new java.util.Date().getTime());
          objprp1.setexpregdat(s);
   objprp1.setexpteccod(Integer.parseInt(request.getParameter("drptec")));
        objprp1.setexpusrcod(ucod);
        obj1.save_rec(objprp1);
          //send email containing pwd
          String to = request.getParameter("txtexpeml");
   String from = "ranamother@gmail.com";
   String host = "mail.connectzone.in";
              Properties properties = System.getProperties();
   properties.setProperty("mail.smtp.host", host);
              Session mailSession = Session.getDefaultInstance(properties);
   try {
       MimeMessage message = new MimeMessage(mailSession);
      message.setFrom(new InternetAddress(from));
      message.addRecipient(Message.RecipientType.TO,
                               new InternetAddress(to));
      message.setSubject("Password Information");
      message.setText("Your Password is "+pwd);
      Transport.send(message);
   } catch (MessagingException mex) {
      PrintWriter out=response.getWriter();
      out.println(mex.getMessage());
   }
          //
          response.sendRedirect("admin/frmexp.jsp");
          }
          catch(Exception exp)
          {
            PrintWriter out=response.getWriter();
      out.println(exp.getMessage());   
          }
      }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
