import buslogic.clsexp;
import buslogic.clsexpprp;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class prfserv extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        clsexp obj=new clsexp();
        clsexpprp objprp=new clsexpprp();
      if(ServletFileUpload.isMultipartContent(request))
        {
             HttpSession ses=request.getSession();
        ServletFileUpload upload=new ServletFileUpload();
            try
            {
    FileItemIterator itr=upload.getItemIterator(request);
                while(itr.hasNext())
                {
                    FileItemStream item=itr.next();
                    if(item.isFormField())
                    {
                            String nam=item.getFieldName();
                            InputStream is=item.openStream();
                            byte[] b=new byte[is.available()];
                            is.read(b);
                            String s=new String(b);
                            if(nam.equals("txtprf"))
                                objprp.setexpprf(s);
                    }
                    else
                    { 
                      InputStream is=item.openStream();
                      if(is.available()>0)
                      {
                 String path=getServletContext().getRealPath("/");
                      fileupl.upload("exppics",path, item);
                      objprp.setexppic(item.getName());
                      }
                    }
                }
                objprp.setexpcod(Integer.parseInt(ses.getAttribute("ucod").toString()));
                obj.upd_rec(objprp);
                 response.sendRedirect("expert/frmprf.jsp");
            }
            catch(Exception exp)
            {       
                PrintWriter out=response.getWriter();
                out.println(exp.getMessage());
            }
        }     
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
