import buslogic.clstec;
import buslogic.clstecprp;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.jboss.weld.context.http.Http;

public class tecserv extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       if(request.getParameter("btnsub")!=null)
       {
            clstec obj=new clstec();
            clstecprp objprp=new clstecprp();
            objprp.settecnam(request.getParameter("txttec"));
            obj.save_rec(objprp);
       }
       if(request.getParameter("btnupd")!=null)
       {
           clstec obj=new clstec();
           clstecprp objprp=new clstecprp();
           objprp.settecnam(request.getParameter("txttec"));
           HttpSession ses=request.getSession(false);
           objprp.setteccod(Integer.parseInt(ses.getAttribute("tcod").toString()));
           obj.upd_rec(objprp);
           ses.removeAttribute("tcod");
       }
       if(request.getParameter("tcod")!=null)
       {
           clstec obj=new clstec();
           clstecprp objprp=new  clstecprp();
           objprp.setteccod(Integer.parseInt(request.getParameter("tcod")));
           obj.del_rec(objprp);
       }
       response.sendRedirect("admin/frmtec.jsp");
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
