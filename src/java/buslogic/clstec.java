

package buslogic;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author csgroup
 */
public class clstec 
{
     CallableStatement stm;
    public void save_rec(clstecprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call instec(?)}");
            stm.setString(1,p.gettecnam());           
            stm.execute();
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
            
        }
    }
    public void upd_rec(clstecprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call updtec(?,?)}");
             stm.setInt(1,p.getteccod());
            stm.setString(2,p.gettecnam());         
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }    
    public List<clstecprp> disp_rec()
    {
        List<clstecprp> arr=new ArrayList<clstecprp>();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call disptec()}");
            ResultSet rs=stm.executeQuery();
            
            while(rs.next())
            {
                clstecprp p=new clstecprp();
                p.setteccod(rs.getInt("teccod"));
                p.settecnam(rs.getString("tecnam"));
               
                
                arr.add(p);
            }
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return arr;
    }
  public  clstecprp find_rec(int tcod)
    {
        clstecprp p=new clstecprp();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call findtec(?)}");
            stm.setInt(1, tcod);
            ResultSet rs=stm.executeQuery();
            if(rs.next())
            {
                p.setteccod(rs.getInt("teccod"));
                p.settecnam(rs.getString("tecnam"));
                
            }
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return p;
    }
   
 public void del_rec(clstecprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call deltec(?)}");
            stm.setInt(1,p.getteccod());
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }
}