/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buslogic;

import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author xxx
 */
public class clsusrpln{
    
        CallableStatement stm;
    public void save_rec(clsusrplnprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call insusrpln(?,?,?)}");
            stm.setInt(1,p.getusrplnusrcod());    
            stm.setInt(2,p.getusrplnplncod()); 
            stm.setDate(3,p.getusrplndat()); 
            
            
            stm.execute();
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
            
        }
    }
    public void upd_rec(clsusrplnprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call updusrpln(?,?,?,?)}");
             stm.setInt(1,p.getusrplncod());
            stm.setInt(2,p.getusrplnusrcod());    
            stm.setInt(3,p.getusrplnplncod()); 
            stm.setDate(4,p.getusrplndat()); 
                       
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }    
    public List<clsusrplnprp> disp_rec()
    {
        List<clsusrplnprp> arr=new ArrayList<clsusrplnprp>();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call dispusrpln()}");
            ResultSet rs=stm.executeQuery();
            
            while(rs.next())
            {
                clsusrplnprp p=new clsusrplnprp();
                p.setusrplncod(rs.getInt("usrplncod"));
                p.setusrplnusrcod(rs.getInt("usrplnusrcod"));
               
                 p.setusrplnplncod(rs.getInt("usrplnplncod"));
                  p.setusrplndat(rs.getDate("usrplndat"));
                 
                arr.add(p);
            }
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return arr;
    }
  public  clsusrplnprp find_rec(int upcod)
    {
        clsusrplnprp p=new clsusrplnprp();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call findusrpln(?)}");
            stm.setInt(1, upcod);
            ResultSet rs=stm.executeQuery();
            if(rs.next())
            {
                
              p.setusrplncod(rs.getInt("usrplncod"));
                p.setusrplnusrcod(rs.getInt("usrplnusrcod"));
               
                 p.setusrplnplncod(rs.getInt("usrplnplncod"));
                  p.setusrplndat(rs.getDate("usrplndat"));
                
            }
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return p;
    }
   
 public void del_rec(clsusrplnprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call delusrpln(?)}");
            stm.setInt(1,p.getusrplncod());
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }

 
}
