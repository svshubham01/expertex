/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buslogic;

import java.sql.Date;

/**
 *
 * @author xxx
 */
public interface intrep {
       public void setrepcod(int rcod);
    public void setrepdat(Date rdat);
    public void setrepqstcod(int rqstcod);
    public void setrepusrcod(int rusrcod);
    public void setrepdsc(String rdsc);
    public void setrepatt(String ratt);
    public void setrepsts(String rsts);
    
    
    public int getrepcod();
    public Date getrepdat();
    public int getrepqstcod();
    public int getrepusrcod();
    public String getrepdsc();
    public String getrepatt();
    public String getrepsts();
    
}
