/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buslogic;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author xxx
 */
public class clsexp {
    
     CallableStatement stm;
    public void save_rec(clsexpprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call insexp(?,?,?,?,?,?)}");
            stm.setString(1,p.getexpnam());    
            stm.setString(2,p.getexpprf()); 
            stm.setString(3,p.getexppic()); 
               stm.setInt(4,p.getexpteccod()); 
                  stm.setDate(5,p.getexpregdat());
                     stm.setInt(6,p.getexpusrcod()); 
            
            stm.execute();
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
            
        }
    }
    public void upd_rec(clsexpprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call updexp(?,?,?)}");
             stm.setInt(1,p.getexpcod());  
            stm.setString(3,p.getexpprf()); 
            stm.setString(2,p.getexppic());  
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }    
    public List<clsexpprp> disp_rec()
    {
        List<clsexpprp> arr=new ArrayList<clsexpprp>();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call dispexp()}");
            ResultSet rs=stm.executeQuery();
            
            while(rs.next())
            {
                clsexpprp p=new clsexpprp();
                p.setexpcod(rs.getInt("expcod"));
                p.setexpnam(rs.getString("expnam"));
               
                 p.setexpprf(rs.getString("expprf"));
                  p.setexppic(rs.getString("exppic"));
                    p.setexpteccod(rs.getInt("expteccod"));
                      p.setexpregdat(rs.getDate("expregdat"));
                        p.setexpusrcod(rs.getInt("expusrcod"));
                arr.add(p);
            }
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return arr;
    }
  public  clsexpprp find_rec(int ecod)
    {
        clsexpprp p=new clsexpprp();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call findexp(?)}");
            stm.setInt(1, ecod);
            ResultSet rs=stm.executeQuery();
            if(rs.next())
            {
                
               p.setexpcod(rs.getInt("expcod"));
                p.setexpnam(rs.getString("expnam"));
               
                 p.setexpprf(rs.getString("expprf"));
                  p.setexppic(rs.getString("exppic"));
                    p.setexpteccod(rs.getInt("expteccod"));
                      p.setexpregdat(rs.getDate("expregdat"));
                        p.setexpusrcod(rs.getInt("expusrcod"));
              
                
            }
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return p;
    }
   
 public void del_rec(clsexpprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call delexp(?)}");
            stm.setInt(1,p.getexpcod());
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }

    
}
