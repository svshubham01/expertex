/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buslogic;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author xxx
 */
public class clsexpqst {
       CallableStatement stm;
    public void save_rec(clsexpqstprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call insexpqst(?,?,?,?)}");
            stm.setInt(1,p.getexpqstexpcod());    
            stm.setInt(2,p.getexpqstqstcod()); 
            stm.setInt(3,p.getexpqstrat());
            stm.setString(4,p.getexpqstfed()); 
            
            
            
            
            stm.execute();
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
            
        }
    }
    public void upd_rec(clsexpqstprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call updexpqst(?,?,?,?,?)}");
             stm.setInt(1,p.getexpqstcod());
             stm.setInt(2,p.getexpqstexpcod());    
            stm.setInt(3,p.getexpqstqstcod()); 
            stm.setInt(4,p.getexpqstrat());
            stm.setString(5,p.getexpqstfed());
             
                       
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }    
    public List<clsexpqstprp> disp_rec()
    {
        List<clsexpqstprp> arr=new ArrayList<clsexpqstprp>();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call dispexpqst()}");
            ResultSet rs=stm.executeQuery();
            
            while(rs.next())
            {
                clsexpqstprp p=new clsexpqstprp();
                p.setexpqstcod(rs.getInt("expqstcod"));
                p.setexpqstexpcod(rs.getInt("expqstexpcod"));
               
                 p.setexpqstqstcod(rs.getInt("expqstqstcod"));
                  p.setexpqstrat(rs.getInt("expqstrat"));
                   p.setexpqstfed(rs.getString("expqstfed"));
                   
                 
                arr.add(p);
            }
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return arr;
    }
  public  clsexpqstprp find_rec(int eqcod)
    {
        clsexpqstprp p=new clsexpqstprp();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call findexpqst(?)}");
            stm.setInt(1, eqcod);
            ResultSet rs=stm.executeQuery();
            if(rs.next())
            {
                
               p.setexpqstcod(rs.getInt("expqstcod"));
                p.setexpqstexpcod(rs.getInt("expqstexpcod"));
               
                 p.setexpqstqstcod(rs.getInt("expqstqstcod"));
                  p.setexpqstrat(rs.getInt("expqstrat"));
                   p.setexpqstfed(rs.getString("expqstfed"));
                
            }
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return p;
    }
   
 public void del_rec(clsexpqstprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call delexpqst(?)}");
            stm.setInt(1,p.getexpqstcod());
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }

}
