package buslogic;

import com.sun.xml.ws.org.objectweb.asm.Type;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;

public class clsusr {
  
     CallableStatement stm;
    public int save_rec(clsusrprp p) throws SQLException
    {
        int ucod=0;
        
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call insusr(?,?,?,?,?)}");
            stm.setString(1,p.getusreml());    
            stm.setString(2,p.getusrpwd()); 
            stm.setString(3,p.getusrrol()); 
      stm.registerOutParameter(4, Types.INTEGER);
      stm.setString(5,p.getusrpic()); 
            stm.execute();
            ucod=stm.getInt(4);
            obj.disconnect();
     
        return ucod;
    }
    public void upd_rec(clsusrprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call updusr(?,?,?,?,?)}");
             stm.setInt(1,p.getusrcod());
            stm.setString(2,p.getusreml());    
            stm.setString(3,p.getusrpwd()); 
            stm.setString(4,p.getusrrol()); 
                        stm.setString(5,p.getusrpic()); 
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }

public ArrayList logincheck(String eml,String pwd)
{
     ArrayList arr=new ArrayList();
         try {
             clscon obj=clscon.getInstance();
             obj.connect();
             stm=obj.con.prepareCall("{call logincheck(?,?,?,?)}");
             stm.setString(1, eml);
             stm.setString(2, pwd);
             stm.registerOutParameter(3, Types.INTEGER);
             stm.registerOutParameter(4, Types.CHAR,1);
             stm.execute();
            
             int cod=stm.getInt(3);
            arr.add(cod);
            arr.add(stm.getString(4).charAt(0));
             obj.disconnect();
         } catch (SQLException ex) {
             Logger.getLogger(clsusr.class.getName()).log(Level.SEVERE, null, ex);
         }
     return arr;
}

public int chkplnsts(int ucod)
{
    int cod=0;
         try {
             clscon obj=clscon.getInstance();
             obj.connect();
             stm=obj.con.prepareCall("{call chkplnsts(?,?)}");
             stm.setInt(1, ucod);
             stm.registerOutParameter(2, Types.INTEGER);
             stm.execute();
            
             cod=stm.getInt(2);
             obj.disconnect();
         } catch (SQLException ex) {
             Logger.getLogger(clsusr.class.getName()).log(Level.SEVERE, null, ex);
         }
     return cod;
}
    public List<clsusrprp> disp_rec()
    {
        List<clsusrprp> arr=new ArrayList<clsusrprp>();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call dispusr()}");
            ResultSet rs=stm.executeQuery();
            
            while(rs.next())
            {
                clsusrprp p=new clsusrprp();
                p.setusrcod(rs.getInt("usrcod"));
                p.setusreml(rs.getString("usreml"));
               
                 p.setusrpwd(rs.getString("usrpwd"));
                  p.setusrrol(rs.getString("usrrol"));
                     p.setusrpic(rs.getString("usrpic"));
                arr.add(p);
            }
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return arr;
    }
  public  clsusrprp find_rec(int ucod)
    {
        clsusrprp p=new clsusrprp();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call findusr(?)}");
            stm.setInt(1, ucod);
            ResultSet rs=stm.executeQuery();
            if(rs.next())
            {
                
               p.setusrcod(rs.getInt("usrcod"));
                p.setusreml(rs.getString("usreml"));
               
                 p.setusrpwd(rs.getString("usrpwd"));
                  p.setusrrol(rs.getString("usrrol"));
                   p.setusrpic(rs.getString("usrpic"));
                
            }
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return p;
    }
   
 public void del_rec(clsusrprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call delusr(?)}");
            stm.setInt(1,p.getusrcod());
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }

    
}
