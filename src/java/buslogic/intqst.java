/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buslogic;

import java.sql.Date;


/**
 *
 * @author xxx
 */
public interface intqst {
      public void setqstcod(int qcod);
    public void setqstpstdat(Date qpstdat);
    public void setqsttit(String qtit);
    public void setqstdsc(String qdsc);
    public void setqstteccod(int qteccod);
    public void setqstusrcod(int qusrcod);
    public void setqststs(String qsts);
    public void setqstatt(String qatt);
    
    public int getqstcod();
    public Date getqstpstdat();
    public String getqsttit();
    public String getqstdsc();
    public int getqstteccod();
    public int getqstusrcod();
    public String getqststs();
    public String getqstatt();
    
}
