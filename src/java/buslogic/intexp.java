/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buslogic;

import java.sql.Date;

/**
 *
 * @author xxx
 */
public interface intexp {
      public void setexpcod(int ecod);
    public void setexpnam(String enam);
    public void setexpprf(String eprf);
    public void setexppic(String epic);
    public void setexpteccod(int eteccod);
    public void setexpregdat(Date eregdat);
     public void setexpusrcod(int eusrcod);
    
    public int getexpcod();
    public String getexpnam();
    public String getexpprf();
    public String getexppic();
     public int getexpteccod();
      public Date getexpregdat();
       public int getexpusrcod();

}
