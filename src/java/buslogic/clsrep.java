/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buslogic;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author xxx
 */
public class clsrep {
           CallableStatement stm;
    public void save_rec(clsrepprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call insrep(?,?,?,?,?,?)}");
            stm.setDate(1,p.getrepdat());    
            stm.setInt(2,p.getrepqstcod()); 
            stm.setInt(3,p.getrepusrcod());
            stm.setString(4,p.getrepdsc()); 
             stm.setString(5,p.getrepatt()); 
              stm.setString(6,p.getrepsts()); 
            
            
            
            
            
            stm.execute();
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
            
        }
    }
    public void upd_rec(clsrepprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call updrep(?,?,?,?,?,?)}");
             stm.setInt(1,p.getrepcod());
                stm.setDate(2,p.getrepdat());    
            stm.setInt(3,p.getrepqstcod()); 
            stm.setInt(4,p.getrepusrcod());
            stm.setString(5,p.getrepdsc()); 
             stm.setString(6,p.getrepatt()); 
              stm.setString(7,p.getrepsts()); 
             
                       
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }    
    public List<clsrepprp> disp_rec()
    {
        List<clsrepprp> arr=new ArrayList<clsrepprp>();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call disprep()}");
            ResultSet rs=stm.executeQuery();
            
            while(rs.next())
            {
                clsrepprp p=new clsrepprp();
                p.setrepcod(rs.getInt("repcod"));
                p.setrepdat(rs.getDate("repdat"));
               
                 p.setrepqstcod(rs.getInt("repqstcod"));
                  p.setrepusrcod(rs.getInt("repusrcod"));
                   p.setrepdsc(rs.getString("repdsc"));
                    p.setrepatt(rs.getString("repatt"));
                     p.setrepsts(rs.getString("repsts"));
                   
                 
                arr.add(p);
            }
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return arr;
    }
  public  clsrepprp find_rec(int rcod)
    {
        clsrepprp p=new clsrepprp();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call findrep(?)}");
            stm.setInt(1, rcod);
            ResultSet rs=stm.executeQuery();
            if(rs.next())
            {
                
               p.setrepcod(rs.getInt("repcod"));
                p.setrepdat(rs.getDate("repdat"));
               
                 p.setrepqstcod(rs.getInt("repqstcod"));
                  p.setrepusrcod(rs.getInt("repusrcod"));
                   p.setrepdsc(rs.getString("repdsc"));
                    p.setrepatt(rs.getString("repatt"));
                     p.setrepsts(rs.getString("repsts"));
                   
            }
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return p;
    }
   
 public void del_rec(clsrepprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call delrep(?)}");
            stm.setInt(1,p.getrepcod());
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }

}
