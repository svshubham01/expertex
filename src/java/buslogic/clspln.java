/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buslogic;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author xxx
 */
public class clspln {
 
     CallableStatement stm;
    public void save_rec(clsplnprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call inspln(?,?,?)}");
            stm.setString(1,p.getplnnam());    
            stm.setFloat(2,p.getplncst()); 
            stm.setInt(3,p.getplnnoq()); 
            
            stm.execute();
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
            
        }
    }
    public void upd_rec(clsplnprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call updpln(?,?,?,?)}");
             stm.setInt(1,p.getplncod());
            stm.setString(2,p.getplnnam());    
            stm.setFloat(3,p.getplncst()); 
            stm.setInt(4,p.getplnnoq());          
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }    
    public List<clsplnprp> disp_rec()
    {
        List<clsplnprp> arr=new ArrayList<clsplnprp>();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call disppln()}");
            ResultSet rs=stm.executeQuery();
            
            while(rs.next())
            {
                clsplnprp p=new clsplnprp();
                p.setplncod(rs.getInt("plncod"));
                p.setplnnam(rs.getString("plnnam"));
               
                 p.setplncst(rs.getFloat("plncst"));
                  p.setplnnoq(rs.getInt("plnnoq"));
                arr.add(p);
            }
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return arr;
    }
  public  clsplnprp find_rec(int pcod)
    {
        clsplnprp p=new clsplnprp();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call findpln(?)}");
            stm.setInt(1, pcod);
            ResultSet rs=stm.executeQuery();
            if(rs.next())
            {
                
                p.setplncod(rs.getInt("plncod"));
                p.setplnnam(rs.getString("plnnam"));
               
                 p.setplncst(rs.getFloat("plncst"));
                  p.setplnnoq(rs.getInt("plnnoq"));
              
                
            }
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return p;
    }
   
 public void del_rec(clsplnprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call delpln(?)}");
            stm.setInt(1,p.getplncod());
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }

}
