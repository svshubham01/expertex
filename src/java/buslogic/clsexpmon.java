/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buslogic;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author xxx
 */
public class clsexpmon {
              CallableStatement stm;
    public void save_rec(clsexpmonprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call insexpmon(?,?,?)}");
            stm.setString(1,p.getexpmonmon());    
            stm.setString(2,p.getexpmonyer()); 
            stm.setInt(3,p.getexpmonexpcod());
          
            
            
            
            
            
            stm.execute();
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
            
        }
    }
    public void upd_rec(clsexpmonprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call updexpmon(?,?,?,?)}");
             stm.setInt(1,p.getexpmoncod());
              stm.setString(2,p.getexpmonmon());    
            stm.setString(3,p.getexpmonyer()); 
            stm.setInt(4,p.getexpmonexpcod());
             
                       
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }    
    public List<clsexpmonprp> disp_rec()
    {
        List<clsexpmonprp> arr=new ArrayList<clsexpmonprp>();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call dispexpmon()}");
            ResultSet rs=stm.executeQuery();
            
            while(rs.next())
            {
                clsexpmonprp p=new clsexpmonprp();
                p.setexpmoncod(rs.getInt("expmoncod"));
                p.setexpmonmon(rs.getString("expmonmon"));
               
                 p.setexpmonyer(rs.getString("expmonyer"));
                  p.setexpmonexpcod(rs.getInt("expmonexpcod"));
                  
                   
                 
                arr.add(p);
            }
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return arr;
    }
  public  clsexpmonprp find_rec(int emcod)
    {
        clsexpmonprp p=new clsexpmonprp();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call findexpmon(?)}");
            stm.setInt(1, emcod);
            ResultSet rs=stm.executeQuery();
            if(rs.next())
            {
                
               p.setexpmoncod(rs.getInt("expmoncod"));
                p.setexpmonmon(rs.getString("expmonmon"));
               
                 p.setexpmonyer(rs.getString("expmonyer"));
                  p.setexpmonexpcod(rs.getInt("expmonexpcod"));
            }
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return p;
    }
   
 public void del_rec(clsexpmonprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call delexpmon(?)}");
            stm.setInt(1,p.getexpmoncod());
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }

}
