/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buslogic;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author xxx
 */
public class clsqst {
       CallableStatement stm;
    public int save_rec(clsqstprp p)
    {
        int cod=0;
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call insqst(?,?,?,?,?,?,?,?)}");
            stm.setDate(1,p.getqstpstdat());    
            stm.setString(2,p.getqsttit()); 
            stm.setString(3,p.getqstdsc());
            stm.setInt(4,p.getqstteccod()); 
            stm.setInt(5,p.getqstusrcod());
            stm.setString(6,p.getqststs());
            stm.setString(7,p.getqstatt());
            stm.registerOutParameter(8, Types.INTEGER);
            stm.execute();
            cod=stm.getInt(8);
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
            
        }
        return cod;
    }
    public void upd_rec(clsqstprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call updqst(?,?,?,?,?,?,?,?)}");
             stm.setInt(1,p.getqstcod());
             stm.setDate(2,p.getqstpstdat());    
            stm.setString(3,p.getqsttit()); 
            stm.setString(4,p.getqstdsc());
            stm.setInt(5,p.getqstteccod()); 
            stm.setInt(6,p.getqstusrcod());
            stm.setString(7,p.getqststs());
            stm.setString(8,p.getqstatt());
             
                       
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }    
    public List<clsqstprp> disp_rec()
    {
        List<clsqstprp> arr=new ArrayList<clsqstprp>();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call dispqst()}");
            ResultSet rs=stm.executeQuery();
            
            while(rs.next())
            {
                clsqstprp p=new clsqstprp();
                p.setqstcod(rs.getInt("qstcod"));
                p.setqstpstdat(rs.getDate("qstpstdat"));
               
                 p.setqsttit(rs.getString("qsttit"));
                  p.setqstdsc(rs.getString("qstdsc"));
                   p.setqstteccod(rs.getInt("qstteccod"));
                    p.setqstusrcod(rs.getInt("qstusrcod"));
                     p.setqststs(rs.getString("qststs"));
                      p.setqstatt(rs.getString("qstatt"));
                 
                arr.add(p);
            }
            obj.disconnect();
            
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return arr;
    }
  public  clsqstprp find_rec(int qcod)
    {
        clsqstprp p=new clsqstprp();
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call findqst(?)}");
            stm.setInt(1, qcod);
            ResultSet rs=stm.executeQuery();
            if(rs.next())
            {
                
               p.setqstcod(rs.getInt("qstcod"));
                p.setqstpstdat(rs.getDate("qstpstdat"));
               
                 p.setqsttit(rs.getString("qsttit"));
                  p.setqstdsc(rs.getString("qstdsc"));
                   p.setqstteccod(rs.getInt("qstteccod"));
                    p.setqstusrcod(rs.getInt("qstusrcod"));
                     p.setqststs(rs.getString("qststs"));
                      p.setqstatt(rs.getString("qstatt"));
                
            }
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
        return p;
    }
   
 public void del_rec(clsqstprp p)
    {
        try
        {
            clscon obj=clscon.getInstance();
            obj.connect();
            stm=obj.con.prepareCall("{call delqst(?)}");
            stm.setInt(1,p.getqstcod());
            stm.execute();
            obj.disconnect();
        }
        catch(Exception exp)
        {
            exp.printStackTrace();
        }
    }

 
}
