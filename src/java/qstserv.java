import buslogic.clsexpqst;
import buslogic.clsexpqstprp;
import buslogic.clsqst;
import buslogic.clsqstprp;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class qstserv extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        clsqst obj=new clsqst();
        clsqstprp objprp=new clsqstprp();
        if(ServletFileUpload.isMultipartContent(request))
        {
            String sts="";
            int ecod=0;
             HttpSession ses=request.getSession();
        ServletFileUpload upload=new ServletFileUpload();
            try
            {
    FileItemIterator itr=upload.getItemIterator(request);
                while(itr.hasNext())
                {
                    FileItemStream item=itr.next();
                    if(item.isFormField())
                    {
                            String nam=item.getFieldName();
                            InputStream is=item.openStream();
                            byte[] b=new byte[is.available()];
                            is.read(b);
                            String s=new String(b);
                            if(nam.equals("txtqsttit"))
                                objprp.setqsttit(s);
                            else if(nam.equals("drptec"))
                                objprp.setqstteccod(Integer.parseInt(s));
                            else if(nam.equals("txtdsc"))
                                objprp.setqstdsc(s);
                            else if(nam.equals("r1"))
                            {
                                sts=s;
                                objprp.setqststs(s);
                            }
                            else if( sts=="E" && nam.equals("drpexp"))
                                ecod=Integer.parseInt(s);
                    }
                    else
                    { 
                      InputStream is=item.openStream();
                      if(is.available()>0)
                      {
                 String path=getServletContext().getRealPath("/");
                      fileupl.upload("qstatt",path, item);
                      objprp.setqstatt(item.getName());
                      }
                    }
                }
                objprp.setqstusrcod(Integer.parseInt(ses.getAttribute("ucod").toString()));
      java.sql.Date s = new java.sql.Date(new java.util.Date().getTime());
                objprp.setqstpstdat(s);
               int qcod= obj.save_rec(objprp);
                if(sts.equals("E"))
                {
                    clsexpqst obj1=new clsexpqst();
                    clsexpqstprp objprp1=new clsexpqstprp();
                    objprp1.setexpqstexpcod(ecod);
                    objprp1.setexpqstrat(0);
                    objprp1.setexpqstfed("");
                    objprp1.setexpqstqstcod(qcod);
                    obj1.save_rec(objprp1);
                }
                 response.sendRedirect("frmmyqst.jsp");
            }
            catch(Exception exp)
            {       
                PrintWriter out=response.getWriter();
                out.println(exp.getMessage());
            }
        }     
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
