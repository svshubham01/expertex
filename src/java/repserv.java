import buslogic.clsqst;
import buslogic.clsqstprp;
import buslogic.clsrep;
import buslogic.clsrepprp;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class repserv extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        clsrep obj=new clsrep();
        clsrepprp objprp=new clsrepprp();
        if(ServletFileUpload.isMultipartContent(request))
        {
             HttpSession ses=request.getSession();
        ServletFileUpload upload=new ServletFileUpload();
            try
            {
    FileItemIterator itr=upload.getItemIterator(request);
                while(itr.hasNext())
                {
                    FileItemStream item=itr.next();
                    if(item.isFormField())
                    {
                            String nam=item.getFieldName();
                            InputStream is=item.openStream();
                            byte[] b=new byte[is.available()];
                            is.read(b);
                            String s=new String(b);
                            if(nam.equals("txtdsc"))
                                objprp.setrepdsc(s);  
                    }
                    else
                    { 
                      InputStream is=item.openStream();
                      if(is.available()>0)
                      {
                 String path=getServletContext().getRealPath("/");
                      fileupl.upload("repatt",path, item);
                      objprp.setrepatt(item.getName());
                      }
                    }
                }
                objprp.setrepusrcod(Integer.parseInt(ses.getAttribute("ucod").toString()));
      java.sql.Date s = new java.sql.Date(new java.util.Date().getTime());
                objprp.setrepdat(s);
                objprp.setrepqstcod(Integer.parseInt(ses.getAttribute("qcod").toString()));
                objprp.setrepsts("N");
                obj.save_rec(objprp);
                 response.sendRedirect("frmqstdet.jsp");
            }
            catch(Exception exp)
            {       
                PrintWriter out=response.getWriter();
                out.println(exp.getMessage());
            }
        }     
        
        
        
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
