<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib  uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!DOCTYPE html>
<html lang="en" style=""
 class=" js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths">
 <head>
   
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="author" content="ScriptsBundle">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

  <title>ExpertExchange</title>
  <!-- =-=-=-=-=-=-= Favicons Icon =-=-=-=-=-=-= -->
  <link rel="icon" href="img/favicon.png" type="image/x-icon">

  <!-- =-=-=-=-=-=-= Bootstrap CSS Style =-=-=-=-=-=-= -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="../css/bootstrap.css">
  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/font-awesome.css">
  <link rel="stylesheet" href="../css/et-line-fonts.css">
  <link rel="stylesheet" type="text/css" href="../css/owl.carousel.css">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic|Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="../css/owl.style.css">
  <link href="../css/css" rel="stylesheet" type="text/css">
  <link type="text/css" rel="Stylesheet" href="../css/shCoreDefault.css">
  <link type="text/css" rel="stylesheet" href="../css/animate.min.css">
  <link type="text/css" rel="stylesheet" href="../css/bootstrap-dropdownhover.min.css">
  <!-- JavaScripts -->
  <script src="js/modernizr.js.download"></script>
  <script src="js/jquery.min.js"> </script>
  <script>
      $(document).on('change', '.btn-fileUpload :file', function() {
          
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
      });
      
      $(document).ready( function() {
          $('.btn-fileUpload :file').on('fileselect', function(event, numFiles, label) {
              var input = $(this).parents('.input-group').find(':text'),
                  log = numFiles > 1 ? numFiles + ' files selected' : label;
              if( input.length ) {
                  input.val(log);
              } else {
                  if( log ) alert(log);
              }
          });
      });
      </script>

</head>

<body>
  <!-- =-=-=-=-=-=-= PRELOADER =-=-=-=-=-=-= -->
  <div class="preloader" style="display: none;"><span class="preloader-gif"></span>
  </div>
  <!-- =-=-=-=-=-=-= HEADER =-=-=-=-=-=-= -->
  <div class="top-bar">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-4">
				<ul class="top-nav nav-left">
					<li><a href="../index.jsp">Home</a>
					</li>
					
				</ul>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-6 col-xs-8">
				<ul class="top-nav nav-right">
					<li><a href="#"><i class="fa fa-lock" aria-hidden="true"></i>Logout</a>
					</li>
					
				</ul>
			</div>
		</div>
	</div>
</div>
  <!-- =-=-=-=-=-=-= HEADER Navigation =-=-=-=-=-=-= -->
  <div class="navbar navbar-default">
    <div class="container">
      <!-- header -->
      <div class="navbar-header">
        <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <!-- logo -->
        <a href="#" class="navbar-brand"><img class="img-responsive" alt="" src="../img/Expert.png">
        </a>
       
        <!-- header end -->
        <!-- header end -->
      </div>
      <!-- navigation menu -->
      <div class="navbar-collapse collapse">
        <!-- right bar -->
        <ul class="nav navbar-nav navbar-right">
          <li><a href="frmtec.jsp">Technologies</a>
          </li>
          <li><a href="frmpln.jsp">Plans</a></li>
           <li><a href="frmexp.jsp">Experts</a></li>
            <li><a href="frmreg.jsp">Register Expert</a></li>
        </ul>
      </div>
      <!-- navigation menu end -->
      <!--/.navbar-collapse -->
    </div>
  </div>
  <!-- HEADER Navigation End -->

  <!-- =-=-=-=-=-=-= register Strat =-=-=-=-=-=-= -->


<!-- =-=-=-=-=-=-= register Form start=-=-=-=-=-=-= -->
<section class="section-padding-80 white" id="register">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12">

        <div class="box-panel">

          <!-- buttons top -->
          <!-- <a href="#" class="btn btn-default facebook"><i class="fa fa-facebook icons"></i> Sign Up with Facebook</a> -->
          <!-- <a href="#" class="btn btn-default google"><i class="fa fa-google-plus icons"></i> Sign Up with Google</a> -->
           <a href="#" class="btn btn-default google">Register Expert</a>
          <!-- end buttons top -->
          <br>
           <sql:setDataSource var="dbcon" driver="com.mysql.jdbc.Driver"
                             url="jdbc:mysql://localhost/dbexpchg" user="root" password=""/>
       
          
          <form method="post" action="../expserv">
            <div class="col-sm-6 col-md-6 padd-right">
              <div class="form-group">
                <label>Select Technology</label>
                <select name="drptec" class="form-control">
                    <sql:query dataSource="${dbcon}" var="t">
                        select * from tbtec;
                    </sql:query> 
                    <c:forEach items="${t.rows}" var="r">
                        <option value="${r.teccod}"/> <c:out value="${r.tecnam}"/>
                    </c:forEach>
                </select>
              </div>
                <div class="form-group">
                    <label>Expert Name</label>
                    <input type="text" name="txtexpnam" class="form-control"
                           placeholder="Expert Name"/>
                </div>
                <div class="form-group">
                    <label>Expert Email</label>
                    <input type="text" name="txtexpeml" class="form-control"
                           placeholder="Expert Email"/>
                </div>
            </div>

                     <input type="submit" class="btn btn-primary btn-lg btn-block"
                     value="Submit" name="btnsub"/>  
          </form>
   
        </div>
      </div>

      <div class="clearfix"></div>
    </div>
  </div>
  <!-- end container -->
</section>

  </div>
  <!-- =-=-=-=-=-=-= Main Area End =-=-=-=-=-=-= -->

  <!-- =-=-=-=-=-=-= FOOTER =-=-=-=-=-=-= -->
  <footer class="footer-area">


    <!--Footer Bottom-->
    <div class="footer-copyright">
      <div class="auto-container clearfix">
        <!--Copyright-->
        <div class="copyright text-center Copyright-sec">Copyright 2018 ©  All Rights Reserved</div>
      </div>
    </div>
  </footer>
  <!-- =-=-=-=-=-=-= JQUERY =-=-=-=-=-=-= -->
  <script src="js/jquery.min.js.download"></script>
  <script src="js/bootstrap.min.js.download"></script>
  <script src="js/jquery.smoothscroll.js.download"></script>
  <script type="text/javascript" src="js.js.download"></script>
  <script src="js/jquery.countTo.js.download"></script>
  <script src="js/jquery.waypoints.js.download"></script>
  <script src="js/jquery.appear.min.js.download"></script>
  <script src="js/carousel.min.js.download"></script>
  <script src="js/jquery.stellar.min.js.download"></script>
  <script src="js/bootstrap-dropdownhover.min.js.download"></script>
  <script type="text/javascript" src="js/shCore.js.download"></script>
  <script type="text/javascript" src="js/shBrushPhp.js.download"></script>
  <script src="js/custom.js.download"></script>


<div style="clear: both;"></div></body></html>