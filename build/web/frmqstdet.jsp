<%@page import="buslogic.clsqstprp"%>
<%@page import="buslogic.clsqst"%>
<%@page import="buslogic.clstecprp"%>
<%@page import="buslogic.clstec"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib  uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!DOCTYPE html>
<html lang="en" style=""
 class=" js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths">
 <head>
   
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="author" content="ScriptsBundle">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

  <title>ExpertExchange</title>
  <!-- =-=-=-=-=-=-= Favicons Icon =-=-=-=-=-=-= -->
  <link rel="icon" href="img/favicon.png" type="image/x-icon">

  <!-- =-=-=-=-=-=-= Bootstrap CSS Style =-=-=-=-=-=-= -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/font-awesome.css">
  <link rel="stylesheet" href="css/et-line-fonts.css">
  <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic|Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="css/owl.style.css">
  <link href="css/css" rel="stylesheet" type="text/css">
  <link type="text/css" rel="Stylesheet" href="css/shCoreDefault.css">
  <link type="text/css" rel="stylesheet" href="css/animate.min.css">
  <link type="text/css" rel="stylesheet" href="css/bootstrap-dropdownhover.min.css">
  <!-- JavaScripts -->
  <script src="js/modernizr.js.download"></script>

</head>

<body>
  <!-- =-=-=-=-=-=-= PRELOADER =-=-=-=-=-=-= -->
  <div class="preloader" style="display: none;"><span class="preloader-gif"></span>
  </div>
<div class="top-bar">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-4">
				<ul class="top-nav nav-left">
					<li><a href="index.jsp">Home</a>
					</li>
					
				</ul>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-6 col-xs-8">
				<ul class="top-nav nav-right">
					<li><a href="login.jsp"><i class="fa fa-lock" aria-hidden="true"></i>Login</a>
					</li>
					<li><a href="frmreg.jsp"><i class="fa fa-user-plus" aria-hidden="true"></i>Signup</a>
					</li>
                                     
					
                                        
				</ul>
			</div>
		</div>
	</div>
</div>
  <!-- =-=-=-=-=-=-= HEADER Navigation =-=-=-=-=-=-= -->
  <div class="navbar navbar-default">
    <div class="container">
      <!-- header -->
      <div class="navbar-header">
        <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <!-- logo -->
        <a href="#" class="navbar-brand"><img class="img-responsive" alt="" src="img/Expert.png">
        </a>
        <!-- search form -->

        <!-- header end -->
        <!-- header end -->
      </div>
      <!-- navigation menu -->
      <div class="navbar-collapse collapse">
        <!-- right bar -->
        <ul class="nav navbar-nav navbar-right">
      
         
          </li>
          <li class="dropdown"> <a class="dropdown-toggle " data-hover="dropdown" data-toggle="dropdown" data-animations="fadeInUp">Search Questions <b class="caret"></b></a>
            <ul class="dropdown-menu">
    <sql:setDataSource driver="com.mysql.jdbc.Driver"
                             url="jdbc:mysql://localhost/dbexpchg" user="root" password="" var="dbcon"/>
    <sql:query dataSource="${dbcon}" var="res">
        select * from tbtec;
    </sql:query>
    <c:forEach items="${res.rows}" var="r">
                      <li><a href="frmsrc.jsp?tcod=${r.teccod}">${r.tecnam}</a>
              </li>
    </c:forEach>
            </ul>
          </li>
          <li>
            <div class="btn-nav"><a href="frmpstqst.jsp" class="btn btn-primary btn-small navbar-btn">Post Question</a>
            </div>
          </li>
        </ul>
      </div>
      <!-- navigation menu end -->
      <!--/.navbar-collapse -->
    </div>
  </div>


  <!-- =-=-=-=-=-=-= Main Area =-=-=-=-=-=-= -->
  <div class="main-content-area">
  <section class="section-padding-80 white" id="latest-post">
      <div class="container">
        <!-- title-section -->
        <div class="main-heading text-center">
            <h2>
                <%
                    HttpSession ses=request.getSession(false);
                if(request.getParameter("qcod")!=null)
                {
                    ses.setAttribute("qcod", request.getParameter("qcod"));
                }
                clsqst obj=new clsqst();
                clsqstprp objprp=new clsqstprp();
               objprp= obj.find_rec(Integer.parseInt(ses.getAttribute("qcod").toString()));
                out.println(objprp.getqsttit());
                    %>
            </h2>
            <sql:setDataSource driver="com.mysql.jdbc.Driver"
                             url="jdbc:mysql://localhost/dbexpchg" user="root" password="" var="dbcon"/>
          <div class="slices"><span class="slice"></span><span class="slice"></span><span class="slice"></span>
          </div>
            <p><%
                out.println(objprp.getqstdsc());
                %></p>
        </div>
        <!-- End title-section -->

        <div class="row">
          <!-- Content Area Bar -->
          <form class="margin-top-40" method="post" action="repserv" enctype="multipart/form-data">
            <%
                if(ses.getAttribute("ucod")!=null)
                {
                %>
              <h3>Post Reply</h3>
                  
              <div class="form-group">
                  <label>Reply Description</label><br>
              <textarea name="txtdsc" rows="5" cols="70"></textarea>         
            </div>
              <div class="form-group">
                  <label>Browse Attachment</label><br>
                  <input type="file" name="fileupl"/>
              </div>
              <div class="form-group">
                  <input type="submit" name="btnsub" value="Submit"/>
              </div>
              <%
                  }
                  %>
          </form>
          
          
          <div class="col-md-8 col-sm-12">
            <div class="listing">
              <!-- Question Area Panel -->
              
                  <sql:query dataSource="${dbcon}" var="res">
                      select repcod,repatt,repdat,repdsc,repsts,usreml from tbrep,tbusr where
repusrcod=usrcod and repqstcod=? order by repdat desc
<sql:param value="${sessionScope.qcod}"/>
                  </sql:query>            
<h3>Replies</h3>
          <c:forEach items="${res.rows}" var="p">
                <div class="listing-grid ">
                  <div class="row">
                 
                    <div class="col-md-7 col-sm-8  col-xs-12">
                 
                      <div class="listing-meta">
                          <span><i class="fa fa-clock-o" aria-hidden="true"></i><c:out value="${p.repdat}"/></span>
                        

                      </div>

                    </div>
                 

                    <div class="col-md-10 col-sm-10  col-xs-12">
                        <p><c:out value="${p.repdsc}"/></p>
                      <div class="pull-right tagcloud">
                          <a href="#"><c:out value="${p.usreml}"/></a>
                     
                      </div>
                    </div>

                  </div>
                </div>
                </c:forEach>

              

            </div>
          </div>
       
          <div class="clearfix"></div>
        </div>
      </div>
      <!-- end container -->
    </section>

  </div>
  <!-- =-=-=-=-=-=-= Main Area End =-=-=-=-=-=-= -->

  <!-- =-=-=-=-=-=-= FOOTER =-=-=-=-=-=-= -->
  <footer class="footer-area">



    <!--Footer Bottom-->
    <div class="footer-copyright">
      <div class="auto-container clearfix">
        <!--Copyright-->
        <div class="copyright text-center Copyright-sec">Copyright 2018 ©  All Rights Reserved</div>
      </div>
    </div>
  </footer>

  <!-- =-=-=-=-=-=-= JQUERY =-=-=-=-=-=-= -->
  <script src="js/jquery.min.js.download"></script>
  <script src="js/bootstrap.min.js.download"></script>
  <script src="js/jquery.smoothscroll.js.download"></script>
  <script type="text/javascript" src="js.js.download"></script>
  <script src="js/jquery.countTo.js.download"></script>
  <script src="js/jquery.waypoints.js.download"></script>
  <script src="js/jquery.appear.min.js.download"></script>
  <script src="js/carousel.min.js.download"></script>
  <script src="js/jquery.stellar.min.js.download"></script>
  <script src="js/bootstrap-dropdownhover.min.js.download"></script>
  <script type="text/javascript" src="js/shCore.js.download"></script>
  <script type="text/javascript" src="js/shBrushPhp.js.download"></script>
  <script src="js/custom.js.download"></script>


<div style="clear: both;"></div></body></html>