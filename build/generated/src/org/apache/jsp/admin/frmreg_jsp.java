package org.apache.jsp.admin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class frmreg_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_forEach_var_items;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_out_value_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_sql_query_var_dataSource;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_sql_setDataSource_var_user_url_password_driver_nobody;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_forEach_var_items = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_out_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_sql_query_var_dataSource = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_sql_setDataSource_var_user_url_password_driver_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_forEach_var_items.release();
    _jspx_tagPool_c_out_value_nobody.release();
    _jspx_tagPool_sql_query_var_dataSource.release();
    _jspx_tagPool_sql_setDataSource_var_user_url_password_driver_nobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html lang=\"en\" style=\"\"\n");
      out.write(" class=\" js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths\">\n");
      out.write(" <head>\n");
      out.write("   \n");
      out.write("  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n");
      out.write("  <meta name=\"author\" content=\"ScriptsBundle\">\n");
      out.write("  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1\">\n");
      out.write("\n");
      out.write("  <title>ExpertExchange</title>\n");
      out.write("  <!-- =-=-=-=-=-=-= Favicons Icon =-=-=-=-=-=-= -->\n");
      out.write("  <link rel=\"icon\" href=\"img/favicon.png\" type=\"image/x-icon\">\n");
      out.write("\n");
      out.write("  <!-- =-=-=-=-=-=-= Bootstrap CSS Style =-=-=-=-=-=-= -->\n");
      out.write("  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n");
      out.write("  <link rel=\"stylesheet\" href=\"../css/bootstrap.css\">\n");
      out.write("  <link rel=\"stylesheet\" href=\"../css/style.css\">\n");
      out.write("  <link rel=\"stylesheet\" href=\"../css/font-awesome.css\">\n");
      out.write("  <link rel=\"stylesheet\" href=\"../css/et-line-fonts.css\">\n");
      out.write("  <link rel=\"stylesheet\" type=\"text/css\" href=\"../css/owl.carousel.css\">\n");
      out.write("  <link href=\"http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic|Merriweather:400,300,300italic,400italic,700,700italic\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("\t<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/owl.style.css\">\n");
      out.write("  <link href=\"../css/css\" rel=\"stylesheet\" type=\"text/css\">\n");
      out.write("  <link type=\"text/css\" rel=\"Stylesheet\" href=\"../css/shCoreDefault.css\">\n");
      out.write("  <link type=\"text/css\" rel=\"stylesheet\" href=\"../css/animate.min.css\">\n");
      out.write("  <link type=\"text/css\" rel=\"stylesheet\" href=\"../css/bootstrap-dropdownhover.min.css\">\n");
      out.write("  <!-- JavaScripts -->\n");
      out.write("  <script src=\"js/modernizr.js.download\"></script>\n");
      out.write("  <script src=\"js/jquery.min.js\"> </script>\n");
      out.write("  <script>\n");
      out.write("      $(document).on('change', '.btn-fileUpload :file', function() {\n");
      out.write("          \n");
      out.write("        var input = $(this),\n");
      out.write("            numFiles = input.get(0).files ? input.get(0).files.length : 1,\n");
      out.write("            label = input.val().replace(/\\\\/g, '/').replace(/.*\\//, '');\n");
      out.write("        input.trigger('fileselect', [numFiles, label]);\n");
      out.write("      });\n");
      out.write("      \n");
      out.write("      $(document).ready( function() {\n");
      out.write("          $('.btn-fileUpload :file').on('fileselect', function(event, numFiles, label) {\n");
      out.write("              var input = $(this).parents('.input-group').find(':text'),\n");
      out.write("                  log = numFiles > 1 ? numFiles + ' files selected' : label;\n");
      out.write("              if( input.length ) {\n");
      out.write("                  input.val(log);\n");
      out.write("              } else {\n");
      out.write("                  if( log ) alert(log);\n");
      out.write("              }\n");
      out.write("          });\n");
      out.write("      });\n");
      out.write("      </script>\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("  <!-- =-=-=-=-=-=-= PRELOADER =-=-=-=-=-=-= -->\n");
      out.write("  <div class=\"preloader\" style=\"display: none;\"><span class=\"preloader-gif\"></span>\n");
      out.write("  </div>\n");
      out.write("  <!-- =-=-=-=-=-=-= HEADER =-=-=-=-=-=-= -->\n");
      out.write("  <div class=\"top-bar\">\n");
      out.write("\t<div class=\"container\">\n");
      out.write("\t\t<div class=\"row\">\n");
      out.write("\t\t\t<div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-4\">\n");
      out.write("\t\t\t\t<ul class=\"top-nav nav-left\">\n");
      out.write("\t\t\t\t\t<li><a href=\"../index.jsp\">Home</a>\n");
      out.write("\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t\t</ul>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<div class=\"col-lg-8 col-md-8 col-sm-6 col-xs-8\">\n");
      out.write("\t\t\t\t<ul class=\"top-nav nav-right\">\n");
      out.write("\t\t\t\t\t<li><a href=\"#\"><i class=\"fa fa-lock\" aria-hidden=\"true\"></i>Logout</a>\n");
      out.write("\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t\t</ul>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t</div>\n");
      out.write("\t</div>\n");
      out.write("</div>\n");
      out.write("  <!-- =-=-=-=-=-=-= HEADER Navigation =-=-=-=-=-=-= -->\n");
      out.write("  <div class=\"navbar navbar-default\">\n");
      out.write("    <div class=\"container\">\n");
      out.write("      <!-- header -->\n");
      out.write("      <div class=\"navbar-header\">\n");
      out.write("        <button data-target=\".navbar-collapse\" data-toggle=\"collapse\" class=\"navbar-toggle\" type=\"button\">\n");
      out.write("          <span class=\"icon-bar\"></span>\n");
      out.write("          <span class=\"icon-bar\"></span>\n");
      out.write("          <span class=\"icon-bar\"></span>\n");
      out.write("        </button>\n");
      out.write("        <!-- logo -->\n");
      out.write("        <a href=\"#\" class=\"navbar-brand\"><img class=\"img-responsive\" alt=\"\" src=\"../img/Expert.png\">\n");
      out.write("        </a>\n");
      out.write("       \n");
      out.write("        <!-- header end -->\n");
      out.write("        <!-- header end -->\n");
      out.write("      </div>\n");
      out.write("      <!-- navigation menu -->\n");
      out.write("      <div class=\"navbar-collapse collapse\">\n");
      out.write("        <!-- right bar -->\n");
      out.write("        <ul class=\"nav navbar-nav navbar-right\">\n");
      out.write("          <li><a href=\"frmtec.jsp\">Technologies</a>\n");
      out.write("          </li>\n");
      out.write("          <li><a href=\"frmpln.jsp\">Plans</a></li>\n");
      out.write("           <li><a href=\"frmexp.jsp\">Experts</a></li>\n");
      out.write("            <li><a href=\"frmreg.jsp\">Register Expert</a></li>\n");
      out.write("        </ul>\n");
      out.write("      </div>\n");
      out.write("      <!-- navigation menu end -->\n");
      out.write("      <!--/.navbar-collapse -->\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("  <!-- HEADER Navigation End -->\n");
      out.write("\n");
      out.write("  <!-- =-=-=-=-=-=-= register Strat =-=-=-=-=-=-= -->\n");
      out.write("\n");
      out.write("\n");
      out.write("<!-- =-=-=-=-=-=-= register Form start=-=-=-=-=-=-= -->\n");
      out.write("<section class=\"section-padding-80 white\" id=\"register\">\n");
      out.write("  <div class=\"container\">\n");
      out.write("    <div class=\"row\">\n");
      out.write("      <div class=\"col-sm-12 col-md-12\">\n");
      out.write("\n");
      out.write("        <div class=\"box-panel\">\n");
      out.write("\n");
      out.write("          <!-- buttons top -->\n");
      out.write("          <!-- <a href=\"#\" class=\"btn btn-default facebook\"><i class=\"fa fa-facebook icons\"></i> Sign Up with Facebook</a> -->\n");
      out.write("          <!-- <a href=\"#\" class=\"btn btn-default google\"><i class=\"fa fa-google-plus icons\"></i> Sign Up with Google</a> -->\n");
      out.write("           <a href=\"#\" class=\"btn btn-default google\">Register Expert</a>\n");
      out.write("          <!-- end buttons top -->\n");
      out.write("          <br>\n");
      out.write("           ");
      if (_jspx_meth_sql_setDataSource_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("       \n");
      out.write("          \n");
      out.write("          <form method=\"post\" action=\"../expserv\">\n");
      out.write("            <div class=\"col-sm-6 col-md-6 padd-right\">\n");
      out.write("              <div class=\"form-group\">\n");
      out.write("                <label>Select Technology</label>\n");
      out.write("                <select name=\"drptec\" class=\"form-control\">\n");
      out.write("                    ");
      if (_jspx_meth_sql_query_0(_jspx_page_context))
        return;
      out.write(" \n");
      out.write("                    ");
      if (_jspx_meth_c_forEach_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("                </select>\n");
      out.write("              </div>\n");
      out.write("                <div class=\"form-group\">\n");
      out.write("                    <label>Expert Name</label>\n");
      out.write("                    <input type=\"text\" name=\"txtexpnam\" class=\"form-control\"\n");
      out.write("                           placeholder=\"Expert Name\"/>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"form-group\">\n");
      out.write("                    <label>Expert Email</label>\n");
      out.write("                    <input type=\"text\" name=\"txtexpeml\" class=\"form-control\"\n");
      out.write("                           placeholder=\"Expert Email\"/>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("                     <input type=\"submit\" class=\"btn btn-primary btn-lg btn-block\"\n");
      out.write("                     value=\"Submit\" name=\"btnsub\"/>  \n");
      out.write("          </form>\n");
      out.write("   \n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("\n");
      out.write("      <div class=\"clearfix\"></div>\n");
      out.write("    </div>\n");
      out.write("  </div>\n");
      out.write("  <!-- end container -->\n");
      out.write("</section>\n");
      out.write("\n");
      out.write("  </div>\n");
      out.write("  <!-- =-=-=-=-=-=-= Main Area End =-=-=-=-=-=-= -->\n");
      out.write("\n");
      out.write("  <!-- =-=-=-=-=-=-= FOOTER =-=-=-=-=-=-= -->\n");
      out.write("  <footer class=\"footer-area\">\n");
      out.write("\n");
      out.write("\n");
      out.write("    <!--Footer Bottom-->\n");
      out.write("    <div class=\"footer-copyright\">\n");
      out.write("      <div class=\"auto-container clearfix\">\n");
      out.write("        <!--Copyright-->\n");
      out.write("        <div class=\"copyright text-center Copyright-sec\">Copyright 2018 ©  All Rights Reserved</div>\n");
      out.write("      </div>\n");
      out.write("    </div>\n");
      out.write("  </footer>\n");
      out.write("  <!-- =-=-=-=-=-=-= JQUERY =-=-=-=-=-=-= -->\n");
      out.write("  <script src=\"js/jquery.min.js.download\"></script>\n");
      out.write("  <script src=\"js/bootstrap.min.js.download\"></script>\n");
      out.write("  <script src=\"js/jquery.smoothscroll.js.download\"></script>\n");
      out.write("  <script type=\"text/javascript\" src=\"js.js.download\"></script>\n");
      out.write("  <script src=\"js/jquery.countTo.js.download\"></script>\n");
      out.write("  <script src=\"js/jquery.waypoints.js.download\"></script>\n");
      out.write("  <script src=\"js/jquery.appear.min.js.download\"></script>\n");
      out.write("  <script src=\"js/carousel.min.js.download\"></script>\n");
      out.write("  <script src=\"js/jquery.stellar.min.js.download\"></script>\n");
      out.write("  <script src=\"js/bootstrap-dropdownhover.min.js.download\"></script>\n");
      out.write("  <script type=\"text/javascript\" src=\"js/shCore.js.download\"></script>\n");
      out.write("  <script type=\"text/javascript\" src=\"js/shBrushPhp.js.download\"></script>\n");
      out.write("  <script src=\"js/custom.js.download\"></script>\n");
      out.write("\n");
      out.write("\n");
      out.write("<div style=\"clear: both;\"></div></body></html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_sql_setDataSource_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sql:setDataSource
    org.apache.taglibs.standard.tag.rt.sql.SetDataSourceTag _jspx_th_sql_setDataSource_0 = (org.apache.taglibs.standard.tag.rt.sql.SetDataSourceTag) _jspx_tagPool_sql_setDataSource_var_user_url_password_driver_nobody.get(org.apache.taglibs.standard.tag.rt.sql.SetDataSourceTag.class);
    _jspx_th_sql_setDataSource_0.setPageContext(_jspx_page_context);
    _jspx_th_sql_setDataSource_0.setParent(null);
    _jspx_th_sql_setDataSource_0.setVar("dbcon");
    _jspx_th_sql_setDataSource_0.setDriver("com.mysql.jdbc.Driver");
    _jspx_th_sql_setDataSource_0.setUrl("jdbc:mysql://localhost/dbexpchg");
    _jspx_th_sql_setDataSource_0.setUser("root");
    _jspx_th_sql_setDataSource_0.setPassword("");
    int _jspx_eval_sql_setDataSource_0 = _jspx_th_sql_setDataSource_0.doStartTag();
    if (_jspx_th_sql_setDataSource_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sql_setDataSource_var_user_url_password_driver_nobody.reuse(_jspx_th_sql_setDataSource_0);
      return true;
    }
    _jspx_tagPool_sql_setDataSource_var_user_url_password_driver_nobody.reuse(_jspx_th_sql_setDataSource_0);
    return false;
  }

  private boolean _jspx_meth_sql_query_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sql:query
    org.apache.taglibs.standard.tag.rt.sql.QueryTag _jspx_th_sql_query_0 = (org.apache.taglibs.standard.tag.rt.sql.QueryTag) _jspx_tagPool_sql_query_var_dataSource.get(org.apache.taglibs.standard.tag.rt.sql.QueryTag.class);
    _jspx_th_sql_query_0.setPageContext(_jspx_page_context);
    _jspx_th_sql_query_0.setParent(null);
    _jspx_th_sql_query_0.setDataSource((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${dbcon}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_sql_query_0.setVar("t");
    int[] _jspx_push_body_count_sql_query_0 = new int[] { 0 };
    try {
      int _jspx_eval_sql_query_0 = _jspx_th_sql_query_0.doStartTag();
      if (_jspx_eval_sql_query_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        if (_jspx_eval_sql_query_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
          out = _jspx_page_context.pushBody();
          _jspx_push_body_count_sql_query_0[0]++;
          _jspx_th_sql_query_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
          _jspx_th_sql_query_0.doInitBody();
        }
        do {
          out.write("\n");
          out.write("                        select * from tbtec;\n");
          out.write("                    ");
          int evalDoAfterBody = _jspx_th_sql_query_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
        if (_jspx_eval_sql_query_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
          out = _jspx_page_context.popBody();
          _jspx_push_body_count_sql_query_0[0]--;
      }
      if (_jspx_th_sql_query_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_sql_query_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_sql_query_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_sql_query_0.doFinally();
      _jspx_tagPool_sql_query_var_dataSource.reuse(_jspx_th_sql_query_0);
    }
    return false;
  }

  private boolean _jspx_meth_c_forEach_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:forEach
    org.apache.taglibs.standard.tag.rt.core.ForEachTag _jspx_th_c_forEach_0 = (org.apache.taglibs.standard.tag.rt.core.ForEachTag) _jspx_tagPool_c_forEach_var_items.get(org.apache.taglibs.standard.tag.rt.core.ForEachTag.class);
    _jspx_th_c_forEach_0.setPageContext(_jspx_page_context);
    _jspx_th_c_forEach_0.setParent(null);
    _jspx_th_c_forEach_0.setItems((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${t.rows}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_c_forEach_0.setVar("r");
    int[] _jspx_push_body_count_c_forEach_0 = new int[] { 0 };
    try {
      int _jspx_eval_c_forEach_0 = _jspx_th_c_forEach_0.doStartTag();
      if (_jspx_eval_c_forEach_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("\n");
          out.write("                        <option value=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${r.teccod}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("\"/> ");
          if (_jspx_meth_c_out_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_forEach_0, _jspx_page_context, _jspx_push_body_count_c_forEach_0))
            return true;
          out.write("\n");
          out.write("                    ");
          int evalDoAfterBody = _jspx_th_c_forEach_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_forEach_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_c_forEach_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_c_forEach_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_c_forEach_0.doFinally();
      _jspx_tagPool_c_forEach_var_items.reuse(_jspx_th_c_forEach_0);
    }
    return false;
  }

  private boolean _jspx_meth_c_out_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_forEach_0, PageContext _jspx_page_context, int[] _jspx_push_body_count_c_forEach_0)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:out
    org.apache.taglibs.standard.tag.rt.core.OutTag _jspx_th_c_out_0 = (org.apache.taglibs.standard.tag.rt.core.OutTag) _jspx_tagPool_c_out_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.OutTag.class);
    _jspx_th_c_out_0.setPageContext(_jspx_page_context);
    _jspx_th_c_out_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_forEach_0);
    _jspx_th_c_out_0.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${r.tecnam}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_out_0 = _jspx_th_c_out_0.doStartTag();
    if (_jspx_th_c_out_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_0);
      return true;
    }
    _jspx_tagPool_c_out_value_nobody.reuse(_jspx_th_c_out_0);
    return false;
  }
}
