<%@page import="buslogic.clsusr"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib  uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>

<!DOCTYPE html>
<html lang="en" style=""
 class=" js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths">
 <head>
   
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="author" content="ScriptsBundle">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

  <title>ExpertExchange</title>
  <!-- =-=-=-=-=-=-= Favicons Icon =-=-=-=-=-=-= -->
  <link rel="icon" href="img/favicon.png" type="image/x-icon">

  <!-- =-=-=-=-=-=-= Bootstrap CSS Style =-=-=-=-=-=-= -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/font-awesome.css">
  <link rel="stylesheet" href="css/et-line-fonts.css">
  <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic|Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="css/owl.style.css">
  <link href="css/css" rel="stylesheet" type="text/css">
  <link type="text/css" rel="Stylesheet" href="css/shCoreDefault.css">
  <link type="text/css" rel="stylesheet" href="css/animate.min.css">
  <link type="text/css" rel="stylesheet" href="css/bootstrap-dropdownhover.min.css">
  <!-- JavaScripts -->
  <script src="js/modernizr.js.download"></script>
  <script language="javascript">
      function abc(a)
      {
          window.location="frmpurpln.jsp?pcod="+a;
      }
   </script>
</head>

<body>
  <!-- =-=-=-=-=-=-= PRELOADER =-=-=-=-=-=-= -->
  <div class="preloader" style="display: none;"><span class="preloader-gif"></span>
  </div>
<div class="top-bar">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-4">
				<ul class="top-nav nav-left">
					<li><a href="index.jsp">Home</a>
					</li>
					
				</ul>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-6 col-xs-8">
				<ul class="top-nav nav-right">
					<li><a href="login.jsp"><i class="fa fa-lock" aria-hidden="true"></i>Login</a>
					</li>
					<li><a href="frmreg.jsp"><i class="fa fa-user-plus" aria-hidden="true"></i>Signup</a>
					</li>
                                     
					
                                        
				</ul>
			</div>
		</div>
	</div>
</div>
  <!-- =-=-=-=-=-=-= HEADER Navigation =-=-=-=-=-=-= -->
  <div class="navbar navbar-default">
    <div class="container">
      <!-- header -->
      <div class="navbar-header">
        <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <!-- logo -->
        <a href="#" class="navbar-brand"><img class="img-responsive" alt="" src="img/Expert.png">
        </a>
        <!-- search form -->

        <!-- header end -->
        <!-- header end -->
      </div>
      <!-- navigation menu -->
      <div class="navbar-collapse collapse">
        <!-- right bar -->
        <ul class="nav navbar-nav navbar-right">
      
         
          </li>
          <li class="dropdown"> <a class="dropdown-toggle " data-hover="dropdown" data-toggle="dropdown" data-animations="fadeInUp">Search Questions <b class="caret"></b></a>
            <ul class="dropdown-menu">
    <sql:setDataSource driver="com.mysql.jdbc.Driver"
                             url="jdbc:mysql://localhost/dbexpchg" user="root" password="" var="dbcon"/>
    <sql:query dataSource="${dbcon}" var="res">
        select * from tbtec;
    </sql:query>
    <c:forEach items="${res.rows}" var="r">
                      <li><a href="frmsrc.jsp?tcod=${r.teccod}">${r.tecnam}</a>
              </li>
    </c:forEach>
            </ul>
          </li>
          <li>
            <div class="btn-nav"><a href="frmpstqst.jsp" class="btn btn-primary btn-small navbar-btn">Post Question</a>
            </div>
          </li>
        </ul>
      </div>
      <!-- navigation menu end -->
      <!--/.navbar-collapse -->
    </div>
  </div>
  <!-- HEADER Navigation End -->

  <!-- =-=-=-=-=-=-= post Question Strat =-=-=-=-=-=-= -->
  <div class="main-content-area">
  <section class="page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-7 co-xs-12 text-left">
          <h1>Purchase Plan</h1>
        </div>
        <!-- end col -->
        <div class="col-md-6 col-sm-5 co-xs-12 text-right">
          <div class="bread">
            <ol class="breadcrumb">
              <li><a href="#">Home</a>
              </li>
              <li class="active">Purchase Plan</li>
            </ol>
          </div>
          <!-- end bread -->
        </div>
        <!-- end col -->
      </div>
      <!-- end row -->
    </div>
    <!-- end container -->
  </section>
</div>


<section class="section-padding-80 white" id="post-question">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-8 ">

        <div class="box-panel">

          <h2>Purchase Plan</h2>
          <p></p>
              <sql:setDataSource driver="com.mysql.jdbc.Driver"
                             url="jdbc:mysql://localhost/dbexpchg" user="root" password="" var="dbcon"/>
          <hr>
          <!-- form login -->
          <form class="margin-top-40" method="post" action="purplnserv" >
        
              <div class="form-group">
              <label>Select Plan</label>
              <select  name="drppln" class="questions-category form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true" onchange="abc(this.value);">
                  <sql:query dataSource="${dbcon}" var="res">
                      select * from tbpln;
                  </sql:query>
                  <c:forEach items="${res.rows}" var="r">
                      <option value="${r.plncod}"/><c:out value="${r.plnnam}"/>
                  </c:forEach>
              </select>            
            </div>
              <c:if test="${not empty param.pcod}">
                  <c:set var="pcod" scope="session" value="${param.pcod}"/>
            <div class="form-group">
                <label>Plan Details </label><br>
           <sql:query dataSource="${dbcon}" var="rt">
                      select * from tbpln where plncod=?;
                      <sql:param value="${sessionScope.pcod}"/>
                  </sql:query>
                      <c:forEach items="${rt.rows}" var="t">
                          <b>Cost :</b>$<c:out value="${t.plncst}" /><br>
                      <input type="hidden" name="hidcst" value="<c:out value="${t.plncst}" />"/>
                      <b>No. of questions :</b><c:out value="${t.plnnoq}"/>
                      </c:forEach>
            </div>
            
            
            <button class="btn btn-primary pull-right" name="btnsub">Pay Now</button>
         </c:if>
              </form>
          <!-- form login -->

        </div>
      </div>

      <!-- Blog Right Sidebar -->
      
      <!-- Blog Right Sidebar End -->
      <div class="clearfix"></div>
    </div>
  </div>
  <!-- end container -->
</section>



    <!-- =-=-=-=-=-=-=  post Question End =-=-=-=-=-=-= -->
    <section class="custom-padding" id="clients">
      <div class="container">
        <div class="row">
          <div class="col-md-2 col-xs-6 col-sm-4 client-block">
            <div class="client-item client-item-style-2">
              <a title="Client Logo" href="#"> <img alt="Clients Logo" src="img/client_5.png"> </a>
            </div>

          </div>
          <div class="col-md-2 col-xs-6 col-sm-4 client-block">
            <div class="client-item client-item-style-2">
              <a title="Client Logo" href="#"> <img alt="Clients Logo" src="img/client_6.png"> </a>
            </div>
          </div>
          <div class="col-md-2 col-xs-6 col-sm-4 client-block">
            <div class="client-item client-item-style-2">
              <a title="Client Logo" href="#"> <img alt="Clients Logo" src="img/client_7.png"> </a>
            </div>

          </div>
          <div class="col-md-2 col-xs-6 col-sm-4 client-block">
            <div class="client-item client-item-style-2">
              <a title="Client Logo" href="#"> <img alt="Clients Logo" src="img/client_8.png"> </a>
            </div>

          </div>
          <div class="col-md-2 col-xs-6 col-sm-4 client-block">
            <div class="client-item client-item-style-2">
              <a title="Client Logo" href="#"> <img alt="Clients Logo" src="img/client_9.png"> </a>
            </div>

          </div>
          <div class="col-md-2 col-xs-6 col-sm-4 client-block">
            <div class="client-item client-item-style-2">
              <a title="Client Logo" href="#"> <img alt="Clients Logo" src="img/client_10.png"> </a>
            </div>
          </div>
        </div>
        <!-- Row End -->
      </div>
      <!-- end container -->
    </section>
    <!-- =-=-=-=-=-=-= Our Clients -end =-=-=-=-=-=-= -->
  </div>
  <!-- =-=-=-=-=-=-= Main Area End =-=-=-=-=-=-= -->

  <!-- =-=-=-=-=-=-= FOOTER =-=-=-=-=-=-= -->
  <footer class="footer-area">

    <!--Footer Upper-->
    <div class="footer-content">
      <div class="container">
        <div class="row clearfix">
          <div class="col-md-8 col-md-offset-2">
            <div class="footer-content text-center no-padding margin-bottom-40">
              <div class="logo-footer"><img id="logo-footer" class="center-block" src="img/ExpertFooterLogo.png" alt="">
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus illo vel dolorum soluta consectetur doloribus sit. Delectus non tenetur odit dicta vitae debitis suscipit doloribus. Lorem ipsum dolor sit amet, illo vel.</p>
            </div>
          </div>
          <!--Two 4th column-->
          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="row clearfix">
              <div class="col-lg-7 col-sm-6 col-xs-12 column">
                <div class="footer-widget about-widget">
                  <h2>Our Addres</h2>
                  <ul class="contact-info">
                    <li><span class="icon fa fa-map-marker"></span> E-300, Phase 8A, Industrial Area, Sector 75</li>
                    <li><span class="icon fa fa-phone"></span>095011 07986</li>
                    <li><span class="icon fa fa-map-marker"></span>info@Cssoftsolutions.com</li>
                    <li><span class="icon fa fa-fax"></span> (042) 1234 7777</li>
                  </ul>
                  <div class="social-links-two clearfix">
                    <a href="#" class="facebook img-circle">
                      <span class="fa fa-facebook-f"></span>
                    </a>
                    <a href="#" class="twitter img-circle">
                      <span class="fa fa-twitter"></span>
                    </a>
                    <a href="#" class="google-plus img-circle">
                      <span class="fa fa-google-plus"></span>
                    </a>
                    <a href="#" class="linkedin img-circle">
                      <span class="fa fa-pinterest-p"></span>
                    </a>
                    <a href="#" class="linkedin img-circle">
                      <span class="fa fa-linkedin"></span>
                    </a>
                  </div>
                </div>
              </div>
              <!--Footer Column-->
              <div class="col-lg-5 col-sm-6 col-xs-12 column">
                <h2>Our Service</h2>
                <div class="footer-widget links-widget">
                  <ul>
                    <li><a href="#">Service Section</a>
                    </li>
                    <li><a href="#">Service Section</a>
                    </li>
                    <li><a href="#">Service Section</a>
                    </li>
                    <li><a href="#">Service Section</a>
                    </li>
                    <li><a href="#">Service Section</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!--Two 4th column End-->
          <!--Two 4th column-->
          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="row clearfix">
              <!--Footer Column-->
              <div class="col-lg-7 col-sm-6 col-xs-12 column">
                <div class="footer-widget news-widget">
                  <h2>Latest News</h2>

                  <!--News Post-->
                  <div class="news-post">
                    <div class="icon"></div>
                    <div class="news-content">
                      <figure class="image-thumb"><img src="img/popular-2.jpg" alt="">
                      </figure>
                      <a href="#">If you need a crown or lorem an implant you will pay it gap it</a>
                    </div>
                    <div class="time">january 2, 2018</div>
                  </div>

                  <!--News Post-->
                  <div class="news-post">
                    <div class="icon"></div>
                    <div class="news-content">
                      <figure class="image-thumb"><img src="img/popular-1.jpg" alt="">
                      </figure>
                      <a href="#">If you need a crown or lorem an implant you will pay it gap it</a>
                    </div>
                    <div class="time">january 2, 2018</div>
                  </div>
                </div>
              </div>
              <!--Footer Column-->
              <div class="col-lg-5 col-sm-6 col-xs-12 column">
                <div class="footer-widget links-widget">
                  <h2>Site Links</h2>
                  <ul>
                    <li><a href="#">Login</a>
                    </li>
                    <li><a href="#">Register</a>
                    </li>
                    <li><a href="#">Listing</a>
                    </li>
                    <li><a href="#">Blog</a>
                    </li>
                    <li><a href="#">Contact Us</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!--Two 4th column End-->
        </div>
      </div>
    </div>

    <!--Footer Bottom-->
    <div class="footer-copyright">
      <div class="auto-container clearfix">
        <!--Copyright-->
        <div class="copyright text-center Copyright-sec">Copyright 2018 ©  All Rights Reserved</div>
      </div>
    </div>
  </footer>
  <!-- =-=-=-=-=-=-= JQUERY =-=-=-=-=-=-= -->
  <script src="js/jquery.min.js.download"></script>
  <script src="js/bootstrap.min.js.download"></script>
  <script src="js/jquery.smoothscroll.js.download"></script>
  <script type="text/javascript" src="js.js.download"></script>
  <script src="js/jquery.countTo.js.download"></script>
  <script src="js/jquery.waypoints.js.download"></script>
  <script src="js/jquery.appear.min.js.download"></script>
  <script src="js/carousel.min.js.download"></script>
  <script src="js/jquery.stellar.min.js.download"></script>
  <script src="js/bootstrap-dropdownhover.min.js.download"></script>
  <script type="text/javascript" src="js/shCore.js.download"></script>
  <script type="text/javascript" src="js/shBrushPhp.js.download"></script>
  <script src="js/custom.js.download"></script>


<div style="clear: both;"></div></body></html>