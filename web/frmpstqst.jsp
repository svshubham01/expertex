<%@page import="buslogic.clsusr"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib  uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%
/*    HttpSession ses=request.getSession(false);
    if(ses.getAttribute("ucod")==null)
       response.sendRedirect("login.jsp");
    if(request.getParameter("sts")!=null)
    {
        ses.setAttribute("sts", 'E');
     clsusr obj=new clsusr();
     if(obj.chkplnsts(Integer.parseInt(ses.getAttribute("ucod").toString()))==0)
          response.sendRedirect("frmpurpln.jsp");    
    }
    */
 %>
<!DOCTYPE html>
<html lang="en" style=""
 class=" js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths">
 <head>
   
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="author" content="ScriptsBundle">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

  <title>ExpertExchange</title>
  <!-- =-=-=-=-=-=-= Favicons Icon =-=-=-=-=-=-= -->
  <link rel="icon" href="img/favicon.png" type="image/x-icon">

  <!-- =-=-=-=-=-=-= Bootstrap CSS Style =-=-=-=-=-=-= -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/font-awesome.css">
  <link rel="stylesheet" href="css/et-line-fonts.css">
  <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic|Merriweather:400,300,300italic,400italic,700,700italic" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="css/owl.style.css">
  <link href="css/css" rel="stylesheet" type="text/css">
  <link type="text/css" rel="Stylesheet" href="css/shCoreDefault.css">
  <link type="text/css" rel="stylesheet" href="css/animate.min.css">
  <link type="text/css" rel="stylesheet" href="css/bootstrap-dropdownhover.min.css">
  <!-- JavaScripts -->
  <script src="js/modernizr.js.download"></script>
  <script language="javascript">
      function abc()
      {
          window.location="frmpstqst.jsp?sts=E";
      }
      function xyz(a)
      {
          window.location="frmpstqst.jsp?tcod="+a;
      }
  </script>
</head>

<body>
  <!-- =-=-=-=-=-=-= PRELOADER =-=-=-=-=-=-= -->
  <div class="preloader" style="display: none;"><span class="preloader-gif"></span>
  </div>
<div class="top-bar">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-4">
				<ul class="top-nav nav-left">
					<li><a href="index.jsp">Home</a>
					</li>
					
				</ul>
			</div>
			<div class="col-lg-8 col-md-8 col-sm-6 col-xs-8">
				<ul class="top-nav nav-right">
					<li><a href="login.jsp"><i class="fa fa-lock" aria-hidden="true"></i>Login</a>
					</li>
					<li><a href="frmreg.jsp"><i class="fa fa-user-plus" aria-hidden="true"></i>Signup</a>
					</li>
                                     
					
                                        
				</ul>
			</div>
		</div>
	</div>
</div>
  <!-- =-=-=-=-=-=-= HEADER Navigation =-=-=-=-=-=-= -->
  <div class="navbar navbar-default">
    <div class="container">
      <!-- header -->
      <div class="navbar-header">
        <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <!-- logo -->
        <a href="#" class="navbar-brand"><img class="img-responsive" alt="" src="img/Expert.png">
        </a>
        <!-- search form -->

        <!-- header end -->
        <!-- header end -->
      </div>
      <!-- navigation menu -->
      <div class="navbar-collapse collapse">
        <!-- right bar -->
        <ul class="nav navbar-nav navbar-right">
      
         
          </li>
          <li class="dropdown"> <a class="dropdown-toggle " data-hover="dropdown" data-toggle="dropdown" data-animations="fadeInUp">Search Questions <b class="caret"></b></a>
            <ul class="dropdown-menu">
    <sql:setDataSource driver="com.mysql.jdbc.Driver"
                             url="jdbc:mysql://localhost/dbexpchg" user="root" password="" var="dbcon"/>
    <sql:query dataSource="${dbcon}" var="res">
        select * from tbtec;
    </sql:query>
    <c:forEach items="${res.rows}" var="r">
                      <li><a href="frmsrc.jsp?tcod=${r.teccod}">${r.tecnam}</a>
              </li>
    </c:forEach>
            </ul>
          </li>
          <li>
            <div class="btn-nav"><a href="frmpstqst.jsp" class="btn btn-primary btn-small navbar-btn">Post Question</a>
            </div>
          </li>
        </ul>
      </div>
      <!-- navigation menu end -->
      <!--/.navbar-collapse -->
    </div>
  </div>
  <!-- HEADER Navigation End -->

  <!-- =-=-=-=-=-=-= post Question Strat =-=-=-=-=-=-= -->
  <div class="main-content-area">
  <section class="page-title">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-7 co-xs-12 text-left">
          <h1>Post Your Question</h1>
        </div>
        <!-- end col -->
        <div class="col-md-6 col-sm-5 co-xs-12 text-right">
          <div class="bread">
            <ol class="breadcrumb">
              <li><a href="#">Home</a>
              </li>
              <li class="active">Post Question</li>
            </ol>
          </div>
          <!-- end bread -->
        </div>
        <!-- end col -->
      </div>
      <!-- end row -->
    </div>
    <!-- end container -->
  </section>
</div>


<section class="section-padding-80 white" id="post-question">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-8 ">

        <div class="box-panel">

          <h2>Post Your Question</h2>
          <p></p>
              <sql:setDataSource driver="com.mysql.jdbc.Driver"
                             url="jdbc:mysql://localhost/dbexpchg" user="root" password="" var="dbcon"/>
          <hr>
          <!-- form login -->
          <form class="margin-top-40" method="post" action="qstserv" enctype="multipart/form-data">
                <div class="form-group">
                  <input type="radio" name="r1" value="C"/>Ask Community
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <input type="radio" name="r1" value="E" onchange="abc();"  checked="
                         <c:choose>
                             <c:when test="${not empty sessionScope.sts}">
                                 <c:out value="true"/>
                             </c:when>
                             <c:otherwise>
                                 <c:out value="false"/>
                             </c:otherwise>
                         </c:choose>"/>Ask Expert
              </div>
              <div class="form-group">
              <label>Technology</label>
              <select  name="drptec" class="questions-category form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true" onchange="xyz(this.value);">
                  <sql:query dataSource="${dbcon}" var="res">
                      select * from tbtec;
                  </sql:query>
                  <c:if test="${not empty param.tcod}">
                      <c:set var="tcod" scope="session" value="${param.tcod}"/>
                  </c:if>
                  <c:forEach items="${res.rows}" var="r">
                      <c:choose>
                          <c:when test="${not empty sessionScope.tcod && sessionScope.tcod==r.tcod}">
                              <option value="${r.teccod}" selected  /><c:out value="${r.tecnam}"/>
                          </c:when>
                          <c:otherwise>
                                 <option value="${r.teccod}"/><c:out value="${r.tecnam}"/>
                          </c:otherwise>
                      </c:choose>
                      
                   
                  </c:forEach>
              </select>            
            </div>
                <c:if test="${not empty sessionScope.tcod}">
                    <div class="form-group">
                         <label>Expert</label>
                         <select name="drpexp" class="questions-category form-control select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                             <sql:query var="eres" dataSource="${dbcon}">
                                 select * from tbexp where expteccod=?
                                 <sql:param value="${sessionScope.tcod}"/>
                             </sql:query>
                                 <c:forEach items="${eres.rows}" var="e">
                                     <option value="${e.expcod}"/>
                                   <c:out value="${e.expnam}"/>
                                 </c:forEach>
                         </select>
                         <input type="submit" name="btnprf" value="View Profile">
                    </div>
                </c:if>
            <div class="form-group">
              <label>Question Title</label>
              <input type="text" name="txtqsttit" placeholder="Bootstrap Not Working" class="form-control">
            </div>
            <div class="form-group">
              <label>Question Detials</label>
              <textarea  name="txtdsc" cols="12" rows="12" placeholder="Post Your Question Details Here....." id="message" name="message" class="form-control"></textarea>
            </div>
               <div class="form-group">
              <label>Attachment</label>
              <input type="file" name="filupl" class="form-control">
            </div>
            
            <button class="btn btn-primary pull-right" name="btnsub">Publish Your Question</button>
          </form>
          <!-- form login -->

        </div>
      </div>

      <!-- Blog Right Sidebar -->
      
      <!-- Blog Right Sidebar End -->
      <div class="clearfix"></div>
    </div>
  </div>
  <!-- end container -->
</section>



    <!-- =-=-=-=-=-=-=  post Question End =-=-=-=-=-=-= -->
    <section class="custom-padding" id="clients">
      <div class="container">
        <div class="row">
          <div class="col-md-2 col-xs-6 col-sm-4 client-block">
            <div class="client-item client-item-style-2">
              <a title="Client Logo" href="#"> <img alt="Clients Logo" src="img/client_5.png"> </a>
            </div>

          </div>
          <div class="col-md-2 col-xs-6 col-sm-4 client-block">
            <div class="client-item client-item-style-2">
              <a title="Client Logo" href="#"> <img alt="Clients Logo" src="img/client_6.png"> </a>
            </div>
          </div>
          <div class="col-md-2 col-xs-6 col-sm-4 client-block">
            <div class="client-item client-item-style-2">
              <a title="Client Logo" href="#"> <img alt="Clients Logo" src="img/client_7.png"> </a>
            </div>

          </div>
          <div class="col-md-2 col-xs-6 col-sm-4 client-block">
            <div class="client-item client-item-style-2">
              <a title="Client Logo" href="#"> <img alt="Clients Logo" src="img/client_8.png"> </a>
            </div>

          </div>
          <div class="col-md-2 col-xs-6 col-sm-4 client-block">
            <div class="client-item client-item-style-2">
              <a title="Client Logo" href="#"> <img alt="Clients Logo" src="img/client_9.png"> </a>
            </div>

          </div>
          <div class="col-md-2 col-xs-6 col-sm-4 client-block">
            <div class="client-item client-item-style-2">
              <a title="Client Logo" href="#"> <img alt="Clients Logo" src="img/client_10.png"> </a>
            </div>
          </div>
        </div>
        <!-- Row End -->
      </div>
      <!-- end container -->
    </section>
    <!-- =-=-=-=-=-=-= Our Clients -end =-=-=-=-=-=-= -->
  </div>
  <!-- =-=-=-=-=-=-= Main Area End =-=-=-=-=-=-= -->

  <!-- =-=-=-=-=-=-= FOOTER =-=-=-=-=-=-= -->
  <footer class="footer-area">

    <!--Footer Upper-->
    <div class="footer-content">
      <div class="container">
        <div class="row clearfix">
          <div class="col-md-8 col-md-offset-2">
            <div class="footer-content text-center no-padding margin-bottom-40">
              <div class="logo-footer"><img id="logo-footer" class="center-block" src="img/ExpertFooterLogo.png" alt="">
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus illo vel dolorum soluta consectetur doloribus sit. Delectus non tenetur odit dicta vitae debitis suscipit doloribus. Lorem ipsum dolor sit amet, illo vel.</p>
            </div>
          </div>
          <!--Two 4th column-->
          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="row clearfix">
              <div class="col-lg-7 col-sm-6 col-xs-12 column">
                <div class="footer-widget about-widget">
                  <h2>Our Addres</h2>
                  <ul class="contact-info">
                    <li><span class="icon fa fa-map-marker"></span> E-300, Phase 8A, Industrial Area, Sector 75</li>
                    <li><span class="icon fa fa-phone"></span>095011 07986</li>
                    <li><span class="icon fa fa-map-marker"></span>info@Cssoftsolutions.com</li>
                    <li><span class="icon fa fa-fax"></span> (042) 1234 7777</li>
                  </ul>
                  <div class="social-links-two clearfix">
                    <a href="#" class="facebook img-circle">
                      <span class="fa fa-facebook-f"></span>
                    </a>
                    <a href="#" class="twitter img-circle">
                      <span class="fa fa-twitter"></span>
                    </a>
                    <a href="#" class="google-plus img-circle">
                      <span class="fa fa-google-plus"></span>
                    </a>
                    <a href="#" class="linkedin img-circle">
                      <span class="fa fa-pinterest-p"></span>
                    </a>
                    <a href="#" class="linkedin img-circle">
                      <span class="fa fa-linkedin"></span>
                    </a>
                  </div>
                </div>
              </div>
              <!--Footer Column-->
              <div class="col-lg-5 col-sm-6 col-xs-12 column">
                <h2>Our Service</h2>
                <div class="footer-widget links-widget">
                  <ul>
                    <li><a href="#">Service Section</a>
                    </li>
                    <li><a href="#">Service Section</a>
                    </li>
                    <li><a href="#">Service Section</a>
                    </li>
                    <li><a href="#">Service Section</a>
                    </li>
                    <li><a href="#">Service Section</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!--Two 4th column End-->
          <!--Two 4th column-->
          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="row clearfix">
              <!--Footer Column-->
              <div class="col-lg-7 col-sm-6 col-xs-12 column">
                <div class="footer-widget news-widget">
                  <h2>Latest News</h2>

                  <!--News Post-->
                  <div class="news-post">
                    <div class="icon"></div>
                    <div class="news-content">
                      <figure class="image-thumb"><img src="img/popular-2.jpg" alt="">
                      </figure>
                      <a href="#">If you need a crown or lorem an implant you will pay it gap it</a>
                    </div>
                    <div class="time">january 2, 2018</div>
                  </div>

                  <!--News Post-->
                  <div class="news-post">
                    <div class="icon"></div>
                    <div class="news-content">
                      <figure class="image-thumb"><img src="img/popular-1.jpg" alt="">
                      </figure>
                      <a href="#">If you need a crown or lorem an implant you will pay it gap it</a>
                    </div>
                    <div class="time">january 2, 2018</div>
                  </div>
                </div>
              </div>
              <!--Footer Column-->
              <div class="col-lg-5 col-sm-6 col-xs-12 column">
                <div class="footer-widget links-widget">
                  <h2>Site Links</h2>
                  <ul>
                    <li><a href="#">Login</a>
                    </li>
                    <li><a href="#">Register</a>
                    </li>
                    <li><a href="#">Listing</a>
                    </li>
                    <li><a href="#">Blog</a>
                    </li>
                    <li><a href="#">Contact Us</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <!--Two 4th column End-->
        </div>
      </div>
    </div>

    <!--Footer Bottom-->
    <div class="footer-copyright">
      <div class="auto-container clearfix">
        <!--Copyright-->
        <div class="copyright text-center Copyright-sec">Copyright 2018 ©  All Rights Reserved</div>
      </div>
    </div>
  </footer>
  <!-- =-=-=-=-=-=-= JQUERY =-=-=-=-=-=-= -->
  <script src="js/jquery.min.js.download"></script>
  <script src="js/bootstrap.min.js.download"></script>
  <script src="js/jquery.smoothscroll.js.download"></script>
  <script type="text/javascript" src="js.js.download"></script>
  <script src="js/jquery.countTo.js.download"></script>
  <script src="js/jquery.waypoints.js.download"></script>
  <script src="js/jquery.appear.min.js.download"></script>
  <script src="js/carousel.min.js.download"></script>
  <script src="js/jquery.stellar.min.js.download"></script>
  <script src="js/bootstrap-dropdownhover.min.js.download"></script>
  <script type="text/javascript" src="js/shCore.js.download"></script>
  <script type="text/javascript" src="js/shBrushPhp.js.download"></script>
  <script src="js/custom.js.download"></script>


<div style="clear: both;"></div></body></html>